# shorturl - private URL shortening service

## Running this yourself

Don't.

If you really want to anyway, build `shorturld`, write a `.env` file, and run `shorturld createuser <name>` to generate a token.
Then run the server with `shorturld serve`.