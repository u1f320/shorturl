module github.com/u1f320/shorturl

go 1.16

require (
	emperror.dev/errors v0.8.0
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/georgysavva/scany v0.2.9
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/urfave/cli/v2 v2.3.0
	go.uber.org/zap v1.17.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
