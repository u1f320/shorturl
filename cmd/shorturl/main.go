package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/u1f320/shorturl/cmd/shorturl/cmd"
	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v3"
)

var app = &cli.App{
	Name:  "shorturl",
	Usage: "CLI app to create and manage short links",

	Commands: []*cli.Command{
		cmd.Create,
	},
}

func main() {
	home, err := os.UserHomeDir()
	if err != nil {
		fmt.Println("Error getting home:", err)
		os.Exit(1)
	}

	path := filepath.Join(home, ".shorturl")
	b, err := os.ReadFile(path)
	if err != nil {
		fmt.Println("Error reading config:", err)
		os.Exit(1)
	}

	err = yaml.Unmarshal(b, &cmd.Config)
	if err != nil {
		fmt.Println("Error reading config:", err)
		os.Exit(1)
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
}
