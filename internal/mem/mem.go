// Package mem implements db.DB with an in-memory store.
//
// This obviously isn't intended for production.
// There is no way to persist URLs or users, this is intended for testing and nothing more.
package mem

import (
	"crypto/rand"
	"encoding/base32"
	"encoding/base64"
	"log"
	"sync"

	"github.com/google/uuid"
	"github.com/u1f320/shorturl/db"
)

var urlEncoding = base32.StdEncoding.WithPadding(base32.NoPadding)
var tokenEncoding = base64.URLEncoding

type mem struct {
	urls  map[string]db.URL
	urlMu sync.Mutex

	users   map[uuid.UUID]db.User
	usersMu sync.Mutex

	tokens   map[string]uuid.UUID
	tokensMu sync.Mutex
}

var _ db.DB = (*mem)(nil)

// New returns a new db.DB
func New() (db.DB, error) {
	m := &mem{
		urls:   map[string]db.URL{},
		users:  map[uuid.UUID]db.User{},
		tokens: map[string]uuid.UUID{},
	}

	u, token, err := m.CreateUser("admin")
	if err != nil {
		return nil, err
	}

	log.Printf("Created user %v (ID: %v) with token \"%v\".", u.Username, u.ID, token)
	return m, nil
}

func (mem *mem) RandomURL(userID uuid.UUID, url string) (u db.URL, err error) {
	u.URL = url
	u.ID = uuid.New()

	mem.urlMu.Lock()
	defer mem.urlMu.Unlock()

	for {
		bytes := make([]byte, 32)
		_, err = rand.Read(bytes)
		if err != nil {
			return
		}

		u.Short = urlEncoding.EncodeToString(bytes)
		if _, ok := mem.urls[u.Short]; !ok {
			break
		}
	}

	user, err := mem.User(userID)
	if err != nil {
		return
	}

	u.User = user

	mem.urls[u.Short] = u
	return u, nil
}

func (mem *mem) NamedURL(userID uuid.UUID, short, url string) (u db.URL, err error) {
	u.Short = short
	u.URL = url
	u.ID = uuid.New()

	user, err := mem.User(userID)
	if err != nil {
		return
	}

	u.User = user

	mem.urlMu.Lock()
	mem.urls[u.Short] = u
	mem.urlMu.Unlock()
	return u, nil
}

func (mem *mem) User(id uuid.UUID) (u db.User, err error) {
	mem.usersMu.Lock()
	defer mem.usersMu.Unlock()

	u, ok := mem.users[id]
	if !ok {
		return u, db.ErrNotFound
	}
	return u, nil
}

func (mem *mem) CreateUser(name string) (u db.User, token string, err error) {
	mem.usersMu.Lock()
	defer mem.usersMu.Unlock()
	for _, user := range mem.users {
		if user.Username == name {
			return u, "", db.ErrAlreadyExists
		}
	}

	bytes := make([]byte, 48)
	_, err = rand.Read(bytes)
	if err != nil {
		return
	}

	u = db.User{
		ID:       uuid.New(),
		Username: name,
	}

	token = tokenEncoding.EncodeToString(bytes)

	mem.tokensMu.Lock()
	mem.tokens[token] = u.ID
	mem.tokensMu.Unlock()

	mem.users[u.ID] = u

	return u, token, nil
}

func (mem *mem) ValidateToken(token string) (*db.User, bool, error) {
	mem.tokensMu.Lock()
	defer mem.tokensMu.Unlock()
	id, ok := mem.tokens[token]
	if !ok {
		return nil, false, nil
	}

	u, err := mem.User(id)
	if err != nil {
		return nil, false, err
	}
	return &u, true, nil
}

func (mem *mem) RegenerateToken(id uuid.UUID) (token string, err error) {
	mem.tokensMu.Lock()
	defer mem.tokensMu.Unlock()
	for k, v := range mem.tokens {
		if v == id {
			delete(mem.tokens, k)
			break
		}
	}

	bytes := make([]byte, 48)
	_, err = rand.Read(bytes)
	if err != nil {
		return
	}

	token = tokenEncoding.EncodeToString(bytes)
	mem.tokens[token] = id
	return token, nil
}

func (mem *mem) URL(short string) (u db.URL, err error) {
	mem.urlMu.Lock()
	defer mem.urlMu.Unlock()

	u, ok := mem.urls[short]
	if !ok {
		return u, db.ErrNotFound
	}

	user, err := mem.User(u.User.ID)
	if err != nil {
		return u, db.ErrUserNotFound
	}
	u.User = user

	return u, nil
}

func (*mem) Username(name string) (u db.User, err error) {
	panic("implement me")
}

func (*mem) AllLinks() ([]db.URL, error) {
	panic("implement me")
}

func (*mem) UserLinks(id uuid.UUID) ([]db.URL, error) {
	panic("implement me")
}

func (*mem) DeleteURL(short string) (err error) {
	panic("implement me")
}

func (*mem) DeleteUser(id uuid.UUID) error {
	panic("implement me")
}
